package myOwnApp.Dao;

import myOwnApp.Entities.Person;

import javax.persistence.*;
import java.util.List;

public class PersonDao{
    private EntityManager entityManager;

    public PersonDao(EntityManager em){
        this.entityManager=em;
    }

    public List <Person> getAll(){
        Query query=entityManager.createQuery("SELECT p FROM Person p");
        return query.getResultList();
    }

    public Person createPerson(String name, String surname){
        Person person=new Person();
        person.setName(name);
        person.setSurname(surname);

        entityManager.persist(person);
        return person;

    }


}
