package myOwnApp.Entities;

import javax.persistence.*;

@Entity
@Table(name="addresses")

public class Address {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column(name="city", length=50, nullable=false)
    private String city;

    @Column(name="zipCode", length=50, nullable=false)
    private String zipCode;

    @Column(name="street", length=50, nullable=false)
    private String street;

    @Column(name="homeNumber")
    private int homeNumber;

    public Long getId(){
        return id;
    }

    public void setId(Long id){
        this.id=id;
    }

    public String getCity(){
        return city;
    }

    public void setCity(String city){
        this.city=city;
    }

    public String getZipCode(){
        return zipCode;
    }

    public void setZipCode(String zipCode){
        this.zipCode=zipCode;
    }

    public String getStreet(){
        return street;
    }

    public void setStreet(String street){
        this.street=street;
    }

    public int getHomeNumber(){
        return homeNumber;
    }

    public void setHomeNumber(int homeNumber){
        this.homeNumber=homeNumber;
    }
}
