package myOwnApp.Entities;

import javax.persistence.*;

@Entity
@Table(name="persons")
public class Person{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column(name="name", length=50, nullable=false)
    private String name;

    @Column(name="surname", length=50, nullable=false)
    private String surname;

    @Column(name="age")
    private int age;

    @Column(name="height")
    private int height;

    @Column(name="weight")
    private Double weight;

    @OneToOne(cascade=CascadeType.ALL, orphanRemoval=true)
    private Address address;

    public Long getId(){
        return id;
    }

    public void setId(Long id){
        this.id=id;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name=name;
    }

    public String getSurname(){
        return surname;
    }

    public void setSurname(String surname){
        this.surname=surname;
    }

    public int getAge(){
        return age;
    }

    public void setAge(int age){
        this.age=age;
    }

    public int getHeight(){
        return height;
    }

    public void setHeight(int height){
        this.height=height;
    }

    public Double getWeight(){
        return weight;
    }

    public void setWeight(Double weight){
        this.weight=weight;
    }

    public Address getAddress(){
        return address;
    }

    public void setAddress(Address address){
        this.address=address;
    }
}
