package myOwnApp;

import myOwnApp.Entities.Person;
import myOwnApp.Service.AccountService;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class Main{
    public static void main(String[] args){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("java-dao");
        EntityManager em = entityManagerFactory.createEntityManager();

        AccountService accountService=new AccountService(em);
        em.getTransaction().begin();
        accountService.createPerson("Arkadiusz", "Macyszyn");
        em.getTransaction().commit();
        List <Person> persons=accountService.getAllPersons();
        for (Person p : persons)
            System.out.println("Person:" + p.getName());

        entityManagerFactory.close();
    }
}
