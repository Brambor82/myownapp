package myOwnApp.Service;

import myOwnApp.Dao.PersonDao;
import myOwnApp.Entities.Person;

import javax.persistence.EntityManager;
import java.util.List;

public class AccountService{
    private PersonDao personDao;

    public AccountService(EntityManager em) {
        this.personDao = new PersonDao(em);
    }

    public List<Person> getAllPersons(){
        return personDao.getAll();

    }

    public void createPerson(String name, String surname){
        personDao.createPerson(name,surname);

    }
}
